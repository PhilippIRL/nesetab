const DEFAULT_WALLPAPER = "https://images.unsplash.com/photo-1544111795-fe8b9def73f6?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1920&h=1080&fit=crop&ixid=eyJhcHBfaWQiOjF9"
const STORAGE_KEY = "newtabSettings"

let settings = null
const defaultSettings = { timeFormat: "default", wallpaper: "default" }

loadSettings()
checkUnsplashKey()

const timeLbl = document.getElementById("time")
const backgroundDiv = document.getElementById("background")
const settingsElem = document.getElementById("settings")
const settingsDialog = document.getElementById("settings-dialog")
const settingsButton = document.getElementById("settings-button")
const settingsCloseButton = document.getElementById("settings-close-button")
const timeFormatSelect = document.getElementById("time-format-select")
const wallpaperSelect = document.getElementById("wallpaper-select")
const unsplashMessage = document.getElementById("settings-unsplash-msg")

updateTime()
updateWallpaper()

setInterval(updateTime, 1000)

function updateTime() {
    let timeText = ""

    switch(settings.timeFormat) {
        case "24h":
            timeText = get24HourTimeString()
            break

        default:
            timeText = new Date().toLocaleTimeString()
    }

    timeLbl.innerText = timeText
}

async function updateWallpaper() {
    let wallpaperUrl

    switch(settings.wallpaper) {
        case "unsplash":
            wallpaperUrl = await tryGetRandomUnsplashWallpaper()
            break

        default:
            wallpaperUrl = DEFAULT_WALLPAPER
    }

    backgroundDiv.style.backgroundImage = `url("${wallpaperUrl}")`
}

function get24HourTimeString() {
    const date = new Date()

    const hours = date.getHours().toString().padStart(2, "0")
    const minutes = date.getMinutes().toString().padStart(2, "0")
    const seconds = date.getSeconds().toString().padStart(2, "0")

    return `${hours}:${minutes}:${seconds}`
}

function showSettingsDialog() {
    settingsPrepare()

    settingsElem.style.display = ""
}

function settingsPrepare() {
    inputPrepare(timeFormatSelect, "timeFormat", updateTime)
    inputPrepare(wallpaperSelect, "wallpaper", updateWallpaper)

    if(UNSPLASH_ACCESS_KEY === null) {
        wallpaperSelect.disabled = true
        unsplashMessage.style.display = "block"
    }
}

function inputPrepare(inputElem, settingsKey, onChange) {
    inputElem.value = settings[settingsKey]

    inputElem.onchange = e => {
        settings[settingsKey] = e.target.value
        
        saveSettings()
        onChange()
    }
}

function closeSettingsDialog() {
    settingsElem.classList.add("fadeout")

    setTimeout(() => {
        settingsElem.style.display = "none"
        settingsElem.classList.remove("fadeout")
    }, 500)
}

settingsButton.onclick = () => showSettingsDialog()

settingsElem.onclick = e => {
    if(e.target === settingsElem) {
        closeSettingsDialog()
    }
}
settingsCloseButton.onclick = () => closeSettingsDialog()

function saveSettings() {
    const json = JSON.stringify(settings)

    window.localStorage[STORAGE_KEY] = json
}

function loadSettings() {
    const json = window.localStorage[STORAGE_KEY]

    if(!json) {
        settings = { ...defaultSettings }
    } else {
        settings = JSON.parse(json)
    }
}

async function getRandomUnsplashWallpaper() {
    const cache = await caches.open("unsplash")

    const wallpaperCount = (await cache.keys()).length
    if(wallpaperCount === 0) {
        fetchNewWallpapers(cache)
        
        throw new Error("no wallpapers cached")
    } else if(wallpaperCount < 10) {
        fetchNewWallpapers(cache)
    }

    const cachedWallpapers = await cache.keys()
    const selectedWallpaperRequest = cachedWallpapers[Math.floor(Math.random() * cachedWallpapers.length)]

    const response = await cache.match(selectedWallpaperRequest)

    if(!response || !response.ok) {
        cache.delete(selectedWallpaperRequest)

        throw new Error("invalid wallpaper cached")
    }

    const blob = await response.blob()
    const url = URL.createObjectURL(blob)

    cache.delete(selectedWallpaperRequest)

    return url
}

async function fetchNewWallpapers(cache) {
    if(navigator.connection && navigator.connection.saveData) {
        return
    }

    const apiResponse = await fetch(`https://api.unsplash.com/photos/random?client_id=${encodeURIComponent(UNSPLASH_ACCESS_KEY)}&orientation=landscape&count=30&content_filter=high`)
    const apiJson = await apiResponse.json()

    const images = apiJson.map(image => image.urls.raw + "&fit=crop&w=3840&h=2160")

    await cache.addAll(images)
}

async function tryGetRandomUnsplashWallpaper() {
    try {
        return await getRandomUnsplashWallpaper()
    } catch(e) {
        return DEFAULT_WALLPAPER
    }
}

function checkUnsplashKey() {
    if(!UNSPLASH_ACCESS_KEY && settings.wallpaper === "unsplash") {
        settings.wallpaper = "default"
        
        saveSettings()
    }
}
